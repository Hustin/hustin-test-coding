namespace Testing_2
{
    public enum CardNumber{Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King};
    public enum CardSuite{Hearts, Diamonds, Clubs, Spades};
    public enum CardColor{Black, Red};
    class Card
    {
        private CardNumber number;
        private CardSuite suite;
        private CardColor color; 
        public Card(CardNumber number, CardSuite suite, CardColor color)
        {
            this.number = number;
            this.suite = suite;
            this.color = color;
        }
        public CardNumber Number
        {
            get { return number; }
            set { number = value; }
        }
        public CardSuite Suite
        {
            get { return suite; }
            set { suite = value; }
        }
        public CardColor Color
        {
            get { return color; }
            set { color = value; }
        }
        public string DisplayCard()
        {
            return (number + " of " + suite);
        }
    }
}