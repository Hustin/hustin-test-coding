using System.Collections.Generic;

namespace Testing_2
 {
    class Players
    {
        private string name;
        private List<Card> hand;
        public Players(string name, List<Card> hand)
        {
            this.name = name;
            this.hand = hand;
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public List<Card> Hand
        {
            get { return hand; }
            set { hand = value; } 
        }
    }
 }