﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Testing_2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Card> deck = new List<Card>();
            List<Players> players = new List<Players>();
            MakePlayers(players);
            string response;
            Console.WriteLine("\nDealing...");
            do
            {
                MakeDeck(deck);
                ShuffleDeck(deck);
                DealHand(deck, players);
                Console.WriteLine("\n==============================================================\n");
                RevealHand(players);
                Console.WriteLine("\nType in the number for the command");
                Console.WriteLine("Would you like to redeal? (1. Redeal 2. Exit)");
                response = Console.ReadLine();
                Console.WriteLine("==============================================================\n");
                switch(response){
                    case "1":
                        Console.WriteLine("Redealing...");
                        break;
                    case "2":
                        response = "Exit";
                        break;
                    default:
                        Console.WriteLine("Invalid Command");
                        break;                   
                }
            }while(response !="Exit"); //Menu for the game
        }   
        static void MakePlayers(List<Players> players)
        {
            string answer;
            int numberOfPlayers;
            bool breaker = true;
            do{
                Console.WriteLine("How many players do you have?");
                answer = Console.ReadLine();
                if(Int32.TryParse(answer, out numberOfPlayers))
                {
                    numberOfPlayers = Convert.ToInt32(answer);
                    breaker = false;
                    Console.WriteLine("List of Players Created!");
                }
                else
                {
                    Console.WriteLine("Invalid Answer");
                }
            }while(breaker); //Gets number of players. Only allows number.
            Console.WriteLine("Would you like to make nicknames for the players? (Yes/No)");
            answer = Console.ReadLine();
            //Condition statement to set nicknames for players or not.
            if(answer.ToUpper() == "YES")
            {
                for(int i = 0; i < numberOfPlayers; i+=1)
                {
                    Console.WriteLine("Nickname for Player " + (i+1) + "?");
                    players.Add(new Players(Console.ReadLine(),null));
                }
            }//User inputs nicknames for each player.
            else{
                for(int i = 0; i < numberOfPlayers; i+=1)
                {
                    Console.WriteLine("Player "+(i+1)+" Added.");
                    players.Add(new Players("Player " + (i+1), null));
                }
            }//Sets name automatically to Player #.
            Console.WriteLine("Players have been created.");
        }
        //Creates a new deck
        static void MakeDeck(List<Card> deck)
        {
            deck.Clear();
            CardColor k;
            foreach(CardSuite i in Enum.GetValues(typeof(CardSuite)))
            {
                foreach(CardNumber j in Enum.GetValues(typeof(CardNumber)))
                {
                    if((i.ToString()=="Spades")||(i.ToString()=="Clubs"))
                    {
                        k = CardColor.Black;
                    }
                    else
                    {
                        k = CardColor.Red;
                    }
                    deck.Add(new Card(j, i, k));
                }
            }
        }
        //Shuffles deck by randomly chosen cards a random amount of times
        static void ShuffleDeck(List<Card> deck)
        {
            Random rnd = new Random();
            for(int i = 0; i < rnd.Next(300,500); i+=1)
            {
                int cardOne = rnd.Next(deck.Count);
                int cardTwo = rnd.Next(deck.Count);
                Card swapCard = deck[cardOne];
                deck[cardOne] = deck[cardTwo];
                deck[cardTwo] = swapCard;
            }
        }
        //Takes the first five cards in deck and puts it into the player's hand. 
        static void DealHand(List<Card> deck, List<Players> players)
        {
            foreach(Players element in players){
                element.Hand = deck.GetRange(0,5);                
                deck.RemoveRange(0,5);
                Console.WriteLine("Hand dealt for " + element.Name + ".");
            }
        }
         //Reveals the hands of all five players.
        static void RevealHand(List<Players> players) 
        {
            foreach(Players element in players)
            {
                Console.WriteLine(element.Name + "'s Hand is:");
                foreach(Card i in element.Hand)
                {
                    Console.WriteLine(i.DisplayCard());
                }
                Console.WriteLine();
            }
        }
    }   
}
